require('dotenv').config()
const fs = require('fs')
const csvParser = require('csv-parse')
const downloader = require('m3u8-to-mp4')

const parser = csvParser({
  delimiter: ',',
  from: 2
})

const rs = fs.createReadStream(process.env.ABS_CSV).pipe(parser)

;(async () => {
  var row

  try {
    rs.on('data', async data => {
      rs.pause()
      row = data
      console.log(data)

      const downloadee = new downloader()
      await downloadee.setInputFile(data[2]).setOutputFile(process.env.CSV_OUT + data[0] + '_' + data[1] + '.mp4').start()
      rs.resume()
    })
  } catch (error) {
    fs.appendFileSync(process.env.CSV_OUT + 'error.html', `<h3>${row[0]} - ${row[1]} (URL: ${row[2]})</h3><br>`)
  }
})()
