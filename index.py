import urllib.request
import os
import shutil

my_lessons = [
   #  http://vid.com/vod/mp4:vod/PRV/Yg0WGN_6.mp4/media_b180000_454.ts
    # "http://vid.com/vod/mp4:vod/PRV/Yg0WGN_6.mp4/media_b180000" # replace this with your url
    "https://574d536c8e8fc.streamlock.net/vod2/_definst_/filme/film_1024/V02_ASM_360p50.mp4/media_w192865898_2"
]

lesson_dir = "my_vids"
try:
    shutil.rmtree(lesson_dir)
except:
    print("ok")

os.makedirs(lesson_dir)
os.chdir(lesson_dir)

for lesson, dwn_link in enumerate(my_lessons):
    print("downloading lesson  %d.. " % (lesson), dwn_link)
    file_name = '%04d.mp4' % lesson
    f = open(file_name, 'ab')
    for x in range(0, 1200):
        try:
            rsp = urllib.request.urlopen(dwn_link + "_%04d.ts" % (x) )
        except:
            break
        file_name = '%d.mp4' % lesson
        print("downloading  %d.ts" % (x))
        f.write(rsp.read())
    f.close()

print("done good luck!! ==================  ")

# source: https://stackoverflow.com/questions/22188332/download-ts-files-from-video-stream#answer-45050718
