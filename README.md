# bulk-video-downloader

1. After cloning the project, `cd` into project and run `yarn install`.
2. Populate your csv file according to the sample provided. (`input/sample.csv`)
3. Add a `.env` file with (absolute) input and output paths from the sample env file provided.
4. Run `node index.js` to start downloading.
5. *Notes: Make sure your `.m3u8` links are valid and they should end in `.m3u8` **without** trailing slash `/`.

> Currently, node program only able to download `.m3u8` stream files.
>
> If you have `some-vids.mp4/random-path-name.ts`, edit the link(s) in python file,
>
> and run with `python index.py`.
